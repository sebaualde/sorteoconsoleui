# README #

This is an small console application to make an lottery in a list of people.

### Quick functionality summary? ###

* Show participants
* Add and delete participants
* Make a lottery
* Show the lottery 
* Save the result in a .txt file

### What's the language of the UI? ###

* The language is spanish, but you can download the source code and change it for what you prefer.

### How i get the file .exe ###

* 	You can download the files necessaries in this repository, 
	in the link https://bitbucket.org/sebaualde/sorteoconsoleui/src/master/ConsoleApp/ConsoleAppUI/bin/Release/net6.0/
* To run the app just make double click in the file ConsoleAppUI.exe