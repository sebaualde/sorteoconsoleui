﻿
using ConsoleAppUI;

Methods methods = new();
int option;

do
{
    methods.DisplayMainMenu();
    option = methods.RequestOptionNumber();

    switch (option)
    {
        case 1:

            methods.DisplayParticipantsMenu();

            break;

        case 2:
            methods.DisplayAddDeleteMenu();

            break;

        case 3:
            methods.RuffleOptionsMenu();

            break;

        case 4:         
            methods.DisplayRuffle();

            break;

        case 5:
            methods.SaveRuffleToTxt();

            break;

        case 6:
            break;

        default:
            methods.PrintTextCenter("Opción seleccionada incorrecta...", false);
            Console.ReadKey();
            break;
    }

} while (option != 6);

methods.GoodbyeMessage();
