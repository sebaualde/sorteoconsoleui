﻿using SorteoLibrary;
using SorteoLibrary.Models;
using System.Globalization;
using System.Text;

namespace ConsoleAppUI;

public class Methods
{
    private readonly IAppLogic _appLogic;

    public Methods()
    {
        _appLogic = new AppLogic();
    }

    public void DisplayAppTitle()
    {
        Console.Clear();

        Console.WriteLine("|------------------------------------------------------------------------------------------------------------|");
        Console.WriteLine("|--------------------------------------- SORTEO DE ELECCION NBA " + DateTime.Today.Year + "-----------------------------------------|");
        Console.WriteLine("|------------------------------------------------------------------------------------------------------------|");
    }

    public void DisplayMainMenu()
    {
        DisplayAppTitle();

        PrintMessage("MENÚ:", 5, true, true);
        PrintMessage("1- Mostrar participantes", 5, true);
        PrintMessage("2- Agregar/Eliminar participantes", 5);
        PrintMessage("3- Realizar sorteo", 5);
        PrintMessage("4- Mostrar sorteo", 5);
        PrintMessage("5- Guardar en el excritorio W10: SorteoNBA" + DateTime.Now.Year + ".txt", 5);
        PrintMessage("6- Salir", 5);

    }

    #region Participants

    //display methods menu
    public void DisplayParticipantsMenu()
    {
        Console.Clear();

        PrintMessage("|------------------------------------------------------------------------------------------------------------|");
        PrintMessage("|------------------------------------- LISTA DE PARTICIPANTES PARA " + DateTime.Today.Year + "--------------------------------------|");
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");

        DisplayParticipants();
        PressAnyKeyMessage();

        Console.ReadKey();
    }

    public void DisplayParticipants()
    {
        Console.WriteLine();

        List<PersonModel> participants = _appLogic.GetParticipants();

        if (participants.Count < 1)
        {
            PrintMessage("No hay participantes registrados.", 5);
        }
        else
        {
            for (int i = 0; i < participants.Count; i++)
            {
                int menuNumber = i + 1;
                PrintMessage($" {menuNumber:D2} - {participants[i].Name}", 6);
            }
        }
    }

    public void DisplayNotParticipantYet()
    {
        PrintMessage("No existen participantes registrados todavia.", 4, true);
        PressAnyKeyMessage();

        Console.ReadKey();
    }

    public void DisplayAddDeleteMenu()
    {
        int option;

        do
        {
            AddDeleteOptionsMenu();
            option = RequestOptionNumber();

            switch (option)
            {
                case 1:
                    DisplayParticipantsMenu();

                    break;
                case 2:
                    AddParticipant();

                    break;
                case 3:
                    DeleteParticipant();

                    break;
                case 4:
                    DeleteAllParticipant();

                    break;
                case 5:
                    break;
                default:
                    PrintMessage("El número de opción no es correcto...", 4, true);
                    Console.ReadKey();
                    break;
            }

        } while (option != 5);
    }

    //add Methods
    public void AddDeleteOptionsMenu()
    {
        Console.Clear();
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");
        PrintMessage("|------------------------------------- AGREGAR O ELIMINAR PARTICIPANTES -------------------------------------|");
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");

        PrintMessage("OPCIONES:", 6, true);
        PrintMessage("1- Listar participantes", 5, true);
        PrintMessage("2- Agregar nuevo participante", 5);
        PrintMessage("3- Eliminar un participante", 5);
        PrintMessage("4- Eliminar a TODOS los participantes", 5);
        PrintMessage("5- Volver al menú principal", 5);

    }

    public void AddParticipantTitle()
    {
        Console.Clear();
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");
        PrintMessage("|------------------------------------------ AGREGAR PARTICIPANTE --------------------------------------------|");
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");
    }

    public void AddParticipant()
    {
        bool addMoreParticipants = true;

        do
        {
            int participantsCount = _appLogic.GetParticipantCount();

            AddParticipantTitle();
            DisplayParticipants();
            PrintMessage($" {participantsCount + 1:D2} - Volver", 6, true);
            PrintMessage("Nombre del participante: ", 5, true, false);

            string name = Console.ReadLine().Trim();
            bool isNumber = int.TryParse(name, out int volver);

            if (isNumber && volver == participantsCount + 1) //if user select the number to return in the menu or name is empty return to back menu
            {
                addMoreParticipants = false;
            }
            else
            {
                AddParticipantTitle();
                DisplayParticipants();
                PrintMessage($" {participantsCount + 1:D2} - Volver", 6, true);

                ResponseModel response = _appLogic.AddParticipant(name);

                PrintTextCenter(response.Mensaje, false);
                Console.ReadKey();
            }

        } while (addMoreParticipants);
    }

    //delete methods
    public void DeleteParticipantTitle()
    {
        Console.Clear();
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");
        PrintMessage("|----------------------------------------- ELIMINAR PARTICIPANTES -------------------------------------------|");
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");
    }

    public void DeleteParticipant()
    {
        bool outDelete = false;

        do
        {
            int participantsCount = _appLogic.GetParticipantCount();

            DeleteParticipantTitle();

            if (participantsCount < 1)
            {
                DisplayNotParticipantYet();
                outDelete = true;
            }
            else
            {
                int outOptionNumber = participantsCount + 1;

                DisplayParticipants();
                PrintMessage($" {outOptionNumber:D2} - Volver", 6, true);

                int optionSelected = RequestOptionNumber("Ingrese el número del participante que desea eliminar: ");

                if (optionSelected == outOptionNumber)
                {
                    outDelete = true;
                }
                else
                {
                    if (optionSelected > outOptionNumber || optionSelected < 1)
                    {
                        PrintTextCenter("El número ingresado no es correcto intentelo nuevamente... ", false);
                        Console.ReadKey();
                    }
                    else
                    {
                        PersonModel personSelected = _appLogic.GetParticipant(optionSelected - 1);

                        if (personSelected is not null)
                        {
                            ConfirmDeleteParticipant(personSelected);
                        }             
                    }
                }

            }

        } while (outDelete is false);
    }

    public void ConfirmDeleteParticipant(PersonModel personSelected)
    {
        bool outDeleteConfirm;

        do
        {
            DeleteParticipantTitle();
            DisplayParticipants();

            outDeleteConfirm = ValidateYesOrNotDelete($"¿Esta seguro que desea eliminar al participante: {personSelected.Name}? (s/n): ", personSelected.Position);

        } while (outDeleteConfirm is false);
    }

    public bool ValidateYesOrNotDelete(string message, int? position = null)
    {
        bool output = false;

        PrintMessage(message, 3, true, false);
        _ = char.TryParse(Console.ReadLine().ToLower(), out char YesOrNot);

        if (YesOrNot == 83 || YesOrNot == 115) //83 is S | 115 is s
        {
            ResponseModel response;

            if (position != null)
            {
                response = _appLogic.RemoveParticipant(position.Value);
            }
            else
            {
                response = _appLogic.RemoveAllParticipants();
            }

            PrintTextCenter($"{response.Mensaje}, presione una tecla para continuar...", false);
            Console.ReadKey();
            output = true;
        }
        else if (YesOrNot == 78 || YesOrNot == 110) //83 is N | 110 is n
        {
            output = true;
        }
        else
        {
            PrintTextCenter("Opción incorrecta, seleccione [s] o [n] y presione enter para continuar... ", false);
            Console.ReadKey();
        }

        return output;
    }

    public void DeleteAllParticipantTitle()
    {
        Console.Clear();
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");
        PrintMessage("|--------------------------------- ELIMINAR A TODOS LOS PARTICIPANTES ---------------------------------------|");
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");
    }

    private void DeleteAllParticipant()
    {
        int participantsCount = _appLogic.GetParticipantCount();
        bool outDelete;

        do
        {
            DeleteAllParticipantTitle();

            if (participantsCount < 1)
            {
                DisplayNotParticipantYet();
                outDelete = true;
            }
            else
            {
                DisplayParticipants();
                outDelete = ValidateYesOrNotDelete("¿Esta seguro que desea eliminar a todos los participantes? (s/n): ");
            }
        } while (outDelete is false);
    }

    #endregion

    #region Ruffle

    public void DisplayRuflleTitle()
    {
        Console.Clear();
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");
        PrintMessage("|------------------------------------------- REALIZAR SORTEO ------------------------------------------------|");
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");
    }

    public void RuffleOptionsMenu()
    {
        if (_appLogic.GetParticipantCount() < 1)
        {
            PrintMessage(" No existen participantes registrados todavia.", 4, true);
            PressAnyKeyMessage();
            Console.ReadKey();
        }
        else
        {
            bool keepInRaffle = true;

            do
            {
                DisplayRuflleTitle();
                DisplayParticipants();

                PrintMessage("OPCIONES:", 6, true);
                PrintMessage("1- Realizar sorteo", 5, true);
                PrintMessage("2- Volver al menú principal", 5);

                int option = RequestOptionNumber();

                switch (option)
                {
                    case 1:
                        Raffle();
                        keepInRaffle = false;

                        break;

                    case 2:
                        keepInRaffle = false;
                        break;

                    default:
                        PrintMessage("El número seleccionado no es válido, intentelo de nuevo...", 4, true, false);
                        Console.ReadKey();

                        break;
                }
            } while (keepInRaffle);
        }
    }

    public void Raffle()
    {
        ResponseModel response = _appLogic.MakeARuffle();

        DisplayRuflleTitle();

        Console.WriteLine();
        PrintMessage(response.Mensaje, 4);

        DisplayRuffle();
    }

    public void DisplayRuffle()
    {
        Console.Clear();
        Console.WriteLine("|------------------------------------------------------------------------------------------------------------|");
        Console.WriteLine($"|---------------------------------------- SORTEO DE { DateTime.Now.Year } ----------------------------------------------------|");
        Console.WriteLine("|------------------------------------------------------------------------------------------------------------|");

        List<PersonModel> participants = _appLogic.GetParticipants();

        if (participants.Any())
        {
            if (participants[0].Position == 0)
            {
                PrintMessage("Todavia no se realizo ningún sorteo para mostrar.", 4, true);
                PressAnyKeyMessage();
                Console.ReadKey();
            }
            else
            {
                PrintRuffleTable(participants);
            }
        }
        else
        {
            PrintMessage(" No existen participantes registrados todavia.", 4, true);
            PressAnyKeyMessage();
            Console.ReadKey();
        }
    }

    private void PrintRuffleTable(List<PersonModel> participants)
    {
        Console.WriteLine();
        PrintMessage("   NOMBRE \t|| \t TURNO", 4);

        participants.ForEach(p =>
        {
            PrintMessage($"   {p.Name} \t - \t {p.Position}", 4);
        });

        PressAnyKeyMessage();
        Console.ReadKey();
    }

    #endregion

    #region Save txt

    public void SaveRuffleToTxt()
    {
        ResponseModel response = _appLogic.SaveRuffleTxt();
        if (response.IsSuccess)
        {
            SaveRuffleSuccessfullMessage();
        }
        else
        {
            PrintMessage(response.Mensaje, 5, true);
            PressAnyKeyMessage();
            Console.ReadKey();
        }
    }

    private void SaveRuffleSuccessfullMessage()
    {
        Console.Clear();
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");
        PrintMessage("|------------------------------------- SORTEO GUARDADO EN:  -------------------------------------------------|");
        PrintMessage("|------------------------------------------------------------------------------------------------------------|");

        Console.WriteLine();
        PrintTextCenter(Environment.CurrentDirectory);

        PrintTextCenter("¡¡¡Sorteo guardado exitosamente!!!");
        DisplayParticipants();
        PressAnyKeyMessage();

        Console.ReadKey();
    }

    #endregion

    private void PressAnyKeyMessage()
    {
        PrintTextCenter("Presione una tecla para continuar...", false);
    }

    private void PrintMessage(string message, int tabNumber = 0, bool isSpaceUp = false, bool isWriteLine = true)
    {
        StringBuilder completeMessage = new();

        if (isSpaceUp)
        {
            completeMessage.Append('\n');
        }

        if (tabNumber > 0)
        {
            for (int i = 0; i < tabNumber; i++)
            {
                completeMessage.Append('\t');
            }
        }

        completeMessage.Append(message);

        if (isWriteLine)
        {
            Console.WriteLine(completeMessage.ToString());
        }
        else
        {
            Console.Write(completeMessage.ToString());
        }
    }

    public int RequestOptionNumber(string message = "")
    {
        Console.WriteLine();

        if (string.IsNullOrEmpty(message))
        {
            message = "Ingrese el número de opción: ";
            Console.Write(String.Format("{0," + ((Console.WindowWidth / 2) + message.Length / 2) + "}", $"{message}"));
        }
        else
        {
            Console.Write(String.Format("{0," + ((Console.WindowWidth / 2) + message.Length / 2) + "}", $"{message}"));
        }
        _ = int.TryParse(Console.ReadLine(), out int output);

        return output;
    }

    public void PrintTextCenter(string message, bool isWriteLine = true)
    {
        Console.WriteLine();

        if (isWriteLine)
        {
            Console.WriteLine(String.Format("{0," + ((Console.WindowWidth / 2) + message.Length / 2) + "}", $"{message}"));
        }
        else
        {
            Console.Write(String.Format("{0," + ((Console.WindowWidth / 2) + message.Length / 2) + "}", $"{message}"));

        }
    }

    public void GoodbyeMessage()
    {
        Console.Clear();
        Console.WriteLine("\n\t\t\t|=========================================================================|");
        Console.WriteLine("\t\t\t\t\tDeveloped by Sebauron, que tengas un buen día.");
        Console.Write("\t\t\t\t\t   Presiona una tecla para finalizar...");
        Console.WriteLine("\n\t\t\t|=========================================================================|");
        Console.ReadLine();
    }
}
