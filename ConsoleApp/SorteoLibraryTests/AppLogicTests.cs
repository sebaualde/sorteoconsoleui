﻿using SorteoLibrary;
using SorteoLibrary.Models;
using System.Collections.Generic;
using System.Xml.Linq;
using Xunit;

namespace SorteoLibraryTests;

public class AppLogicTests
{
    private readonly IAppLogic _appLogic = new AppLogic();

    [Theory]
    [InlineData("pepe", "Participante agregado con éxito!!!")]
    [InlineData("seba", "Ya existe un participante con el nombre 'seba', presione una tecla para continuar... ")]
    [InlineData("", "El nombre del participante no puede quedar en blanco, presione una tecla para continuar... ")]
    [InlineData("  ", "El nombre del participante no puede quedar en blanco, presione una tecla para continuar... ")]
    [InlineData(null, "El nombre del participante no puede quedar en blanco, presione una tecla para continuar... ")]
    public void AddParicipantShouldReturnExpectedValue(string name, string expected)
    {
        // Arrange
        ResponseModel response = _appLogic.AddParticipant(name);

        // Act
        string actual = response.Mensaje;

        // Assert
        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData("pepe", false)] //dont exist
    [InlineData("seba", true)] //exist
    public void ExistsParticipantShouldReturnExpectedValue(string name, bool expected)
    {
        //act
        bool actual = _appLogic.ExistsParticipant(name);

        // Assert
        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData(13, false)] //dont exist
    [InlineData(1, true)] //exist seba
    public void GetParticipantShouldReturnExpectedValue(int position, bool expected)
    {
        // Arrange
        PersonModel? response = _appLogic.GetParticipant(position);

        //act
        bool actual = response != null;

        // Assert
        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData(8)]
    public void GetParticipantCountShouldReturnExpectedValue(int expected)
    {
        //act
        int actual = _appLogic.GetParticipantCount();

        // Assert
        Assert.Equal(expected, actual);
    }

    //GetParticipants
    [Fact]
    public void GetParticipantsShouldReturnExpectedValue()
    {
        //act
        List<PersonModel>? actual = _appLogic.GetParticipants();

        // Assert
        Assert.IsType<List<PersonModel>>(actual);
    }

    [Fact]
    public void MakeARuffleShouldReturnExpectedValue()
    {
        // Arrange
        ResponseModel response = _appLogic.MakeARuffle();

        //act
        bool actual = response.IsSuccess;

        // Assert
        Assert.True(actual);
    }

    [Fact]
    public void RemoveAllParticipantsShouldReturnExpectedValue()
    {
        // Arrange
        ResponseModel response = _appLogic.RemoveAllParticipants();

        //act
        bool actual = response.IsSuccess;

        // Assert
        Assert.True(actual);
    }

    [Theory]
    [InlineData( 1, true)] //delete ok seba
    [InlineData( 50, false)] //delete fail, position 50 not exist
    public void RemoveParticipantShouldReturnExpectedValue(int position, bool expected)
    {
        //arrange
        ResponseModel response = _appLogic.RemoveParticipant(position);

        //act
        bool actual = response.IsSuccess;

        // Assert
        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData(true, false, true)] //save file ok
    [InlineData(false, false, false)] //Error, not make a ruffle before
    [InlineData(true, true, false)] //Error, the list of participants are empty

    public void SaveRuffleTxtShouldReturnExpectedValue(bool isNotRuffleError, bool isEmptyListParticipants,  bool expected)
    {
        //arrange
        if (isNotRuffleError)
        {
            _appLogic.MakeARuffle();
        }

        if (isEmptyListParticipants)
        {
            _appLogic.RemoveAllParticipants();
        }

        ResponseModel response = _appLogic.SaveRuffleTxt();

        //act
        bool actual = response.IsSuccess;

        // Assert
        Assert.Equal(expected, actual);
    }
}
