﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SorteoLibrary.Models;

public class ResponseModel
{
    public string Mensaje { get; set; }

    public bool IsSuccess { get; set; }

    public Exception Exception { get; set; }
}
