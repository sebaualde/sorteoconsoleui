﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SorteoLibrary.Models;

public class PersonModel
{
    public string Name { get; set; }

    public int Position { get; set; }
}
