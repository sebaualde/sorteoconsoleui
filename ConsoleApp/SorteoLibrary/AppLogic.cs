﻿using SorteoLibrary.Models;
using System.Xml.Linq;

namespace SorteoLibrary;

public class AppLogic : IAppLogic
{
    private List<PersonModel> _participants = new() {
        new PersonModel() { Name = "Santi", Position = 0},
        new PersonModel() { Name = "Seba", Position = 1},
        new PersonModel() { Name = "Mauri", Position = 2},
        new PersonModel() { Name = "Juanse", Position = 3},
        new PersonModel() { Name = "Alvaro", Position = 4},
        new PersonModel() { Name = "JuanMa", Position = 5},
        new PersonModel() { Name = "Leandro", Position = 6},
        new PersonModel() { Name = "JuanCa", Position = 7},
    };

    public ResponseModel AddParticipant(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
        {
            return new ResponseModel() { Mensaje = $"El nombre del participante no puede quedar en blanco, presione una tecla para continuar... ", IsSuccess = false };
        }
        bool isExists = ExistsParticipant(name);

        if (isExists)
        {
            return new ResponseModel() { Mensaje = $"Ya existe un participante con el nombre '{name}', presione una tecla para continuar... ", IsSuccess = false };
        }
        else
        {
            try
            {
                _participants.Add(new PersonModel() { Name = name, Position = _participants.Count });

                return new ResponseModel() { Mensaje = "Participante agregado con éxito!!!", IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseModel() { Mensaje = "ERROR!!! No se pudo agregar el participante", IsSuccess = false, Exception = ex };
            }
        }

        
    }

    public bool ExistsParticipant(string name)
    {
        return _participants.Where(p => p.Name.ToLower().Trim() == name.ToLower().Trim()).FirstOrDefault() != null;
    }

    public PersonModel GetParticipant(int position)
    {
        return _participants.Where(p => p.Position == position).FirstOrDefault();
    }

    public int GetParticipantCount()
    {
        return _participants.Count;
    }

    public List<PersonModel> GetParticipants()
    {
        return _participants;
    }

    public ResponseModel MakeARuffle()
    {
        int aux = 0;
        Random random = new Random();
        List<int?> raffedNumbers = new();

        try
        {

            for (int i = 0; i < _participants.Count; i++)
            {
                bool isRaffed = true;

                do
                {
                    //first number is the begginig and second is the final of the range
                    aux = random.Next(1, (_participants.Count + 1));

                    isRaffed = raffedNumbers.FirstOrDefault(n => n == aux) != null;

                    if (isRaffed is false)
                    {
                        _participants[i].Position = aux;
                        raffedNumbers.Add(aux);
                    }

                } while (isRaffed);
            }

            _participants = _participants.OrderBy(p => p.Position).ToList();
            return new ResponseModel() { Mensaje = $"¡¡¡SORTEO {DateTime.Now.Year} REALIZADO CON EXITO!!!", IsSuccess = true };
        }
        catch (Exception)
        {
            return new ResponseModel() { Mensaje = $"ERROR!!! No se pudo realizar el sorteo", IsSuccess = false };
        }
    }

    public ResponseModel RemoveAllParticipants()
    {
        try
        {
            _participants.Clear();
            return new ResponseModel() { Mensaje = "Participantes eliminados con éxito!!!", IsSuccess = true };
        }
        catch (Exception ex)
        {
            return new ResponseModel() { Mensaje = "ERROR!!! No se pudo eliminar los participantes", IsSuccess = false, Exception = ex };
        }
    }

    public ResponseModel RemoveParticipant(int position)
    {
        try
        {
            PersonModel participant = GetParticipant(position);
            if (participant is null)
            {
                throw new EntryPointNotFoundException("ERROR!!! Participante no encontrado");
            }

            _participants.Remove(participant);
            ReasignParticipantsPositions();

            return new ResponseModel() { Mensaje = "Participante eliminado con éxito!!!", IsSuccess=true };

        }
        catch(EntryPointNotFoundException ex)
        {
            return new ResponseModel() { Mensaje = ex.Message, IsSuccess = false, Exception = ex };

        }
        catch (Exception ex)
        {
            return new ResponseModel() { Mensaje = "ERROR!!! No se pudo eliminar el participante", IsSuccess = false, Exception = ex };
        }
    }

    public ResponseModel SaveRuffleTxt()
    {
        try
        {
            if (_participants.Any())
            {
                if (_participants[0].Position == 0)
                {
                    throw new NotImplementedException("ERROR!!! Todavia no se realizo ningún sorteo que guardar.");
                }
                else
                {
                    SaveTxtFile();
                }

                return new ResponseModel() { Mensaje = "Sorteo guardado con éxito!!!", IsSuccess = true };
            }
            else
            {
                throw new NotImplementedException("ERROR!!! No existen participantes en la lista para guardar.");
            }
        }
        catch (NotImplementedException ex)
        {
            return new ResponseModel() { Mensaje = ex.Message, IsSuccess = false, Exception = ex };
        }
        catch (Exception ex)
        {
            return new ResponseModel() { Mensaje = "ERROR!!! No se pudo guardar el Sorteo", IsSuccess = false, Exception = ex };
        }
    }

    private void SaveTxtFile()
    {
        using StreamWriter archivo = new(@$"{Environment.CurrentDirectory}\SorteoNBA{ DateTime.Now.Year }.txt");
        archivo.WriteLine($"\t\t\t\t¡¡¡SORTEO {DateTime.Now.Year} REALIZADO CON EXITO!!!");
        archivo.WriteLine();
        archivo.WriteLine("\t\t\t\t   NOMBRE \t|| \t TURNO");

        _participants.ForEach(p =>
        {
            archivo.WriteLine($"\t\t\t\t   {p.Name} \t - \t {p.Position}");
        });
    }

    private void ReasignParticipantsPositions()
    {
        int position = 0;
        _participants.ForEach(p =>
        {
            p.Position = position++;
        });
    }
}