﻿using SorteoLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SorteoLibrary;

public interface IAppLogic
{
    List<PersonModel> GetParticipants();

    PersonModel GetParticipant(int position);

    bool ExistsParticipant(string name);

    int GetParticipantCount();

    ResponseModel AddParticipant(string name);   

    ResponseModel RemoveParticipant(int position);

    ResponseModel RemoveAllParticipants();

    ResponseModel MakeARuffle();

    ResponseModel SaveRuffleTxt();

}
